package programa;

import java.util.InputMismatchException;
import java.util.Scanner;

import clases.Ayuntamiento;
import clases.Castillo;
import clases.Empleado;
import clases.Monumento;
import clases.Templo;

/**
 * Clase Programa que permite al usuario interactuar con la aplicaci�n mediante
 * un men�. Los datos necesarios se introducen por consola.
 * 
 * @author Klara Galuskova, 1� DAW
 */

public class Programa {

	static Scanner input = new Scanner(System.in);
	static Ayuntamiento gestor = new Ayuntamiento();

	public static void main(String[] args) {

		int opcion;

		do {

			opcion = elegirOpcionMenu();

			switch (opcion) {

			case 1:
				pedirDatosEmpleado();
				break;

			case 2:
				pedirDatosMonumento("monumento");
				break;

			case 3:
				pedirDatosMonumento("templo");
				break;

			case 4:
				pedirDatosMonumento("castillo");
				break;

			case 5:
				buscarEmpleado();
				break;

			case 6:
				buscarMonumento();
				break;

			case 7:
				listarEmpleados();
				break;

			case 8:
				listarMonumentos();
				break;

			case 9:
				eliminarEmpleado();
				break;

			case 10:
				eliminarMonumento();
				break;

			case 11:
				asignarEmpleadoMonumento();
				break;

			case 12:
				buscarEmpleadoMonumento();
				break;

			case 13:
				calcularSuperficieTemplo();
				break;

			case 14:
				anadirIvaAlPrecioCastillo();
				break;

			case 0:
				terminarPrograma();
				break;

			default:
				System.out.println("La opci�n '" + opcion + "' no es v�lida");
			}

		} while (opcion != '3');

	}

	/**
	 * M�todo que muestra el men� con las opciones por consola. No recibe nada, y
	 * devuelve la opci�n introducida por teclado. Controla la excepxi�n
	 * {@code java.util.InputMismatchException} por si la opci�n introducida por
	 * teclado no es un n�mero entero.
	 * 
	 * @return la opci�n introducida por teclado (un n�mero entero)F
	 */

	private static int elegirOpcionMenu() {

		int opcion = 0;
		boolean error = false;

		do {

			System.out.println("\n__________________________");
			System.out.println("      AYUNTAMIENTO");
			System.out.println("__________________________");
			System.out.println("Elige una opci�n del men�:");
			System.out.println("	1  - Alta empleado");
			System.out.println("	2  - Alta monumento");
			System.out.println("	3  - Alta templo");
			System.out.println("	4  - Alta castillo");
			System.out.println("	5  - Buscar empleado");
			System.out.println("	6  - Buscar monumento");
			System.out.println("	7  - Listar empleados");
			System.out.println("	8  - Listar monumentos");
			System.out.println("	9  - Eliminar empleado");
			System.out.println("	10 - Eliminar monumento");
			System.out.println("	11 - Asignar empleado a un monumento");
			System.out.println("	12 - Mostrar si el empleado trabaja en el monumento");
			System.out.println("	13 - Calcular superficie de un templo");
			System.out.println("	14 - Asignar IVA al precio de entradas de un castillo");
			System.out.println("	0  - Salir");

			try {
				opcion = input.nextInt();
				error = false;
			} catch (InputMismatchException excepcion) {
				error = true;
				System.out.println("- La opci�n no es v�lida. Tiene que ser un n�mero entero (0-14) -");
			}

			input.nextLine();

		} while (error);

		return opcion;

	}

	/**
	 * M�todo para pedir el DNI por teclado. Devuelve el valor introducido por
	 * teclado.
	 * 
	 * @return la cadena introducida por teclado
	 */

	private static String pedirDni() {

		System.out.println("Introduce el dni:");
		return input.nextLine();

	}

	/**
	 * M�todo para pedir el c�digo del monumento por teclado. Devuelve el valor
	 * introducido por teclado.
	 * 
	 * @return la cadena introducida por teclado
	 */

	private static String pedirCodigo() {

		System.out.println("Introduce el c�digo del monumento:");
		return input.nextLine();

	}

	/**
	 * M�todo que pide los valores necesarios para poder dar de alta a un nuevo
	 * empleado. No recibe nada y no devuelve nada. Se ejecuta tantas veces cu�ntas
	 * quiere el usuario, mientras que la llamada al m�todo
	 * {@code altaMasObjetos(String tipo)} devuelve true. Al tener todos los
	 * valores, hace una llamada al m�todo
	 * {@code altaEmpleado(String nombre, String apellido, String dni)} de la clase
	 * Ayuntamiento pas�ndole los valores recibidos por teclado por par�metro.
	 */

	private static void pedirDatosEmpleado() {

		do {

			System.out.println("Introduce el nombre:");
			String nombre = input.nextLine();
			System.out.println("Introduce el apellido:");
			String apellido = input.nextLine();
			System.out.println("Introduce el dni:");
			String dni = input.nextLine();

			gestor.altaEmpleado(nombre, apellido, dni);

		} while (altaMasObjetos("empleado"));

	}

	/**
	 * M�todo que pide los valores necesarios para poder dar de alta a un nuevo
	 * monumento. No devuelve nada, y recibe el tipo de monumento que queremos crear
	 * (templo, castillo o monumento). Se ejecuta tantas veces cu�ntas quiere el
	 * usuario, mientras que la llamada al m�todo
	 * {@code altaMasObjetos(String tipo)} devuelve true. Si creamos un objeto de la
	 * superclase, y tenemos todos los valores, el m�todo hace una llamada al m�todo
	 * {@code altaMonumento(String codigo, String tipo, double precio)} de la clase
	 * Ayuntamiento pas�ndole los valores recibidos por teclado por par�metro. En
	 * caso de que el tipo de objeto es castillo o templo, hace una llamada al
	 * m�todo {@code pedirDatosTemplo(String codigo, String tipo, double precio)} o
	 * {@code pedirDatosCastillo(String codigo, String tipo, double precio)}, seg�n
	 * el tipo de objeto. Controla la excepci�n
	 * {@code java.util.InputMismatchException}.
	 * 
	 * @param tipoObjeto el tipo de monumento que queremos crear (templo, castillo o
	 *                   monumento)
	 */

	private static void pedirDatosMonumento(String tipoObjeto) {

		do {

			System.out.println("Introduce el c�digo:");
			String codigo = input.nextLine();
			System.out.println("Introduce el tipo (ej. iglesia):");
			String tipo = input.nextLine();

			double precio = 0;
			boolean error = false;

			do {

				System.out.println("Introduce el precio de las entradas:");

				try {
					precio = input.nextDouble();
					error = false;
				} catch (InputMismatchException exception) {
					error = true;
					input.nextLine();
					System.out.println("- El precio no es v�lido. Tiene que ser un n�mero -");
				}

			} while (error);

			input.nextLine();

			if (tipoObjeto.equals("templo")) {

				pedirDatosTemplo(codigo, tipo, precio);

			} else if (tipoObjeto.equals("castillo")) {

				pedirDatosCastillo(codigo, tipo, precio);

			} else {
				gestor.altaMonumento(codigo, tipo, precio);
			}

		} while (altaMasObjetos(tipoObjeto));

	}

	/**
	 * M�todo que pide los valores necesarios para poder dar de alta a un nuevo
	 * objeto de la clase Templo. No devuelve nada, y recibe los par�metros
	 * introducidos por teclado en el m�todo anterior (atributos de la superclase).
	 * Los valores de los atributos propios de la subclase Templo se introducen por
	 * teclado. Una vez que tenemos todos los valores, llama al m�todo
	 * {@code altaTemplo(String codigo, String tipo, double precio, double ancho, double largo, String nombre)}
	 * de la clase Ayuntamiento pas�ndole los valores recibidos por teclado por
	 * par�metro. Controla la excepci�n {@code java.util.InputMismatchException}.
	 * 
	 * @param codigo c�digo del monumento recibido del m�todo anterior
	 * @param tipo   tipo de monumento (ej. iglesia) recibido del m�todo anterior
	 * @param precio precio de las entradas recibido del m�todo anterior
	 */

	private static void pedirDatosTemplo(String codigo, String tipo, double precio) {

		System.out.println("Introduce el nombre:");
		String nombre = input.nextLine();

		boolean error = false;
		double ancho = 0;

		do {

			System.out.println("Introduce el ancho:");

			try {
				ancho = input.nextDouble();
				error = false;
			} catch (InputMismatchException exception) {
				error = true;
				input.nextLine();
				System.out.println("- El ancho no es v�lido. Tiene que ser un n�mero -");
			}

		} while (error);

		error = false;
		double largo = 0;

		do {

			System.out.println("Introduce el largo:");

			try {
				largo = input.nextDouble();
				error = false;
			} catch (InputMismatchException exception) {
				error = true;
				input.nextLine();
				System.out.println("- El largo no es v�lido. Tiene que ser un n�mero -");
			}

		} while (error);

		gestor.altaTemplo(codigo, tipo, precio, ancho, largo, nombre);

		input.nextLine();

	}

	/**
	 * M�todo que pide los valores necesarios para poder dar de alta a un nuevo
	 * objeto de la clase Castillo. No devuelve nada, y recibe los par�metros
	 * introducidos por teclado en el m�todo anterior (atributos de la superclase).
	 * Los valores de los atributos propios de la subclase Castillo se introducen
	 * por teclado. Una vez que tenemos todos los valores, llama al m�todo
	 * {@code altaCastillo(String codigo, String tipo, double precio, String estilo, int siglo, double altura)}
	 * de la clase Ayuntamiento pas�ndole los valores recibidos por teclado por
	 * par�metro. Controla la excepci�n {@code java.util.InputMismatchException}.
	 * 
	 * @param codigo c�digo del monumento recibido del m�todo anterior
	 * @param tipo   tipo de monumento (ej. iglesia) recibido del m�todo anterior
	 * @param precio precio de las entradas recibido del m�todo anterior
	 */

	private static void pedirDatosCastillo(String codigo, String tipo, double precio) {

		System.out.println("Introduce el estilo:");
		String estilo = input.nextLine();

		boolean error = false;
		int siglo = 0;

		do {

			System.out.println("Introduce el siglo:");

			try {
				siglo = input.nextInt();
				error = false;
			} catch (InputMismatchException exception) {
				error = true;
				input.nextLine();
				System.out.println("- El siglo no es v�lido. Tiene que ser un n�mero -");
			}

		} while (error);

		error = false;
		double altura = 0;

		do {

			System.out.println("Introduce la altura:");

			try {
				altura = input.nextDouble();
				error = false;
			} catch (InputMismatchException exception) {
				error = true;
				input.nextLine();
				System.out.println("- La altura no es v�lida. Tiene que ser un n�mero entero -");
			}

		} while (error);

		gestor.altaCastillo(codigo, tipo, precio, estilo, siglo, altura);

		input.nextLine();

	}

	/**
	 * M�todo que recibe el tipo de objeto que estamos creando, y devuelve true o
	 * false, seg�n si queremos crear m�s objetos de este tipo o no. El mensaje se
	 * repite hasta introducir uno de los car�cteres que da por v�lidos
	 * ('s'/'S'/'n'/'N').
	 * 
	 * @param tipo el tipo de objeto que vamos a crear (solo se usa en el mensaje
	 *             que aparece por consola)
	 * @return true al introducir el car�cter 's'/'S', false al introducir el
	 *         car�cter 'n'/'N'
	 */

	private static boolean altaMasObjetos(String tipo) {
		char opcion;

		do {

			System.out.println("�Quieres introducir otro " + tipo + "? (S/N)");
			opcion = input.nextLine().toLowerCase().charAt(0);

			if (opcion != 's' && opcion != 'n') {
				System.out.println("Opci�n '" + opcion + "' no es v�lida. (S/N)");
			}

		} while (opcion != 's' && opcion != 'n');

		if (opcion == 's') {
			return true;
		}

		return false;

	}

	/**
	 * M�todo que llama al m�todo buscarEmpleado() de la clase Ayuntamiento. Despu�s
	 * guarda el objeto que le devuelve este m�todo, y si no es null lo muestra por
	 * consola. El DNI del empleado se pide por teclado mediante la llamada al
	 * m�todo pedirDni(). Si no hay empleados dados de alta, muestra el mensaje
	 * correspondiente. No recibe ni devuelve nada.
	 */

	private static void buscarEmpleado() {

		if (hayEmpleados()) {

			Empleado empleado = gestor.buscarEmpleado(pedirDni());

			if (empleado != null) {
				System.out.println(empleado);
			}

		} else {
			mostrarMensajeListaEmpleadosVacia();
		}

	}

	/**
	 * M�todo que llama al m�todo buscarMonumento() de la clase Ayuntamiento.
	 * Despu�s guarda el objeto que le devuelve este m�todo, y si no es null lo
	 * muestra por consola. El c�digo del monumento se pide por teclado mediante la
	 * llamada al m�todo pedirCodigo(). Si no hay monumentos dados de alta, muestra
	 * el mensaje correspondiente. No recibe ni devuelve nada.
	 */

	private static void buscarMonumento() {

		if (hayMonumentos()) {

			Monumento monumento = gestor.buscarMonumento(pedirCodigo());

			if (monumento != null) {
				System.out.println(monumento);
			}

		} else {
			mostrarMensajeListaMonumentosVacia();
		}

	}

	/**
	 * M�todo que llama al m�todo listarEmpleados() de la clase Ayuntamiento. Si no
	 * hay empleados dados de alta, muestra el mensaje correspondiente. No recibe ni
	 * devuelve nada.
	 */

	private static void listarEmpleados() {

		if (hayEmpleados()) {
			gestor.listarEmpleados();
		} else {
			mostrarMensajeListaEmpleadosVacia();
		}

	}

	/**
	 * M�todo que llama al m�todo listarMonumentos() de la clase Ayuntamiento. Si no
	 * hay monumentos dados de alta, muestra el mensaje correspondiente. No recibe
	 * ni devuelve nada.
	 */

	private static void listarMonumentos() {

		if (hayMonumentos()) {
			gestor.listarMonumentos();
		} else {
			mostrarMensajeListaMonumentosVacia();
		}

	}

	/**
	 * M�todo que llama al m�todo {@code eliminarEmpleado(String dni)} de la clase
	 * Ayuntamiento. No recibe nada y no devuelve nada. El DNI del empleado se
	 * introduce por teclado, mediante la llamada al m�todo pedirDni(). Si no hay
	 * empleados dados de alta, no hace nada y muestra el mensaje correspondiente.
	 */

	private static void eliminarEmpleado() {

		if (hayEmpleados()) {

			String dni = pedirDni();

			if (gestor.eliminarEmpleado(dni)) {
				System.out.println("Empleado '" + dni + "' eliminado");
			} else {
				System.out.println("El empleado con el dni '" + dni + "' no existe");
			}

		} else {
			mostrarMensajeListaEmpleadosVacia();
		}

	}

	/**
	 * M�todo que llama al m�todo {@code eliminarMonumento(String codigoMonumento)}
	 * de la clase Ayuntamiento. No recibe nada y no devuelve nada. El c�digo del
	 * monumento se introduce por teclado, mediante la llamada al m�todo
	 * pedirCodigo(). Si no hay monumentos dados de alta, no hace nada y muestra el
	 * mensaje correspondiente.
	 */

	private static void eliminarMonumento() {

		if (hayMonumentos()) {

			String codigo = pedirCodigo();

			if (gestor.eliminarMonumento(codigo)) {
				System.out.println("Monumento '" + codigo + "' eliminado");
			} else {
				System.out.println("El monumento con el c�digo '" + codigo + "' no existe");
			}

		} else {
			mostrarMensajeListaMonumentosVacia();
		}

	}

	/**
	 * M�todo que llama al m�todo
	 * {@code asignarEmpleadoMonumento(String dni, String codigo)} de la clase
	 * Ayuntamiento. El m�todo no recibe nada y no devuelve nada. Llama a los
	 * m�todos pedirCodigo() y pedirDni(). Hace una llamada al m�todo
	 * {@code asignarEmpleadoMonumento(String dni, String codigo)} de la clase
	 * Ayuntamiento, pas�ndole los valores recibidos por teclado por par�metro. Si
	 * la lista de empleados o de monumentos est� vac�a, muestra los mensajes
	 * correspondientes.
	 */

	private static void asignarEmpleadoMonumento() {

		if (hayMonumentos() && hayEmpleados()) {

			String dni = pedirDni();
			String codigo = pedirCodigo();

			gestor.asignarEmpleadoMonumento(dni, codigo);

		} else {

			if (hayMonumentos()) {
				mostrarMensajeListaEmpleadosVacia();
			} else if (hayEmpleados()) {
				mostrarMensajeListaMonumentosVacia();
			} else {
				mostrarMensajeListasVacias();
			}

		}

	}

	/**
	 * M�todo que llama al m�todo {@code trabajaEnMonumento(String codigo)} de la
	 * clase Monumento. El m�todo no recibe nada y no devuelve nada. Llama a los
	 * m�todos pedirCodigo() y pedirDni(), y despu�s busca los objetos con los
	 * par�metros recibidos por teclado haci�ndo uso de los m�todos
	 * {@code buscarMonumento(String codigo)} y {@code buscarEmpleado(String dni)}
	 * de la clase Ayuntamiento. Despu�s llama al m�todo
	 * {@code trabajaEnMonumento(String codigo)} de la clase Monumento, y muestra
	 * los mensajes correspondientes seg�n lo que devuelve este m�todo (true/false).
	 */

	private static void buscarEmpleadoMonumento() {

		if (hayMonumentos() && hayEmpleados()) {

			String codigo = pedirCodigo();
			String dni = pedirDni();

			Monumento monumento = gestor.buscarMonumento(codigo);
			Empleado empleado = gestor.buscarEmpleado(dni);

			if (monumento != null && empleado != null) {

				if (monumento.trabajaEnMonumento(dni)) {
					System.out.println("El empleado '" + dni + "' trabaja en el monumento '" + codigo + "'");
				} else {
					System.out.println("El empleado '" + dni + "' no est� trabajando en el monumento '" + codigo + "'");
				}

			}

		} else {

			if (hayMonumentos()) {
				mostrarMensajeListaEmpleadosVacia();
			} else if (hayEmpleados()) {
				mostrarMensajeListaMonumentosVacia();
			} else {
				mostrarMensajeListasVacias();
			}

		}

	}

	/**
	 * M�todo que llama al m�todo propio de la subclase Templo que calcula su
	 * superficie, despu�s de hacer una serie de comprobaciones. El m�todo no recibe
	 * nada y no devuelve nada. Llama al m�todo hayMonumentos() para comprobar si
	 * hay monumentos almacenados en la lista de monumentos, despu�s hace una
	 * llamada al m�todo pedirCodigo() y busca el monumento con el m�todo
	 * buscarMonumento(String codigo) pas�ndole el c�digo que hemos introducido por
	 * teclado como par�metro. Controla la excepci�n
	 * {@code java.lang.ClassCastException} cuando un objeto de una clase no se
	 * puede castear a otro clase (por ejemplo el casteo del objeto de tipo Templo a
	 * la clase Castillo, porque no tienen los mismos par�metros). Muestra los
	 * mensajes correspondientes por consola.
	 */

	private static void calcularSuperficieTemplo() {

		if (hayMonumentos()) {

			Monumento monumento = gestor.buscarMonumento(pedirCodigo());

			if (monumento != null) {

				try {
					System.out.println("La superficie es: " + ((Templo) monumento).calcularSuperficie() + " m\u00B2");
				} catch (ClassCastException exception) {
					System.out.println("El monumento no es un templo, no se puede calcular su superficie");
				}

			}

		} else {
			mostrarMensajeListaMonumentosVacia();
		}

	}

	/**
	 * M�todo que llama al m�todo propio de la subclase Castillo que a�ade el IVA al
	 * precio del castillo, despu�s de hacer una serie de comprobaciones. El m�todo
	 * no recibe nada y no devuelve nada. Llama al m�todo hayMonumentos() para
	 * comprobar si hay monumentos almacenados en la lista de monumentos, despu�s
	 * hace una llamada al m�todo pedirCodigo() y busca el monumento con el m�todo
	 * buscarMonumento(String codigo) pas�ndole el c�digo que hemos introducido por
	 * teclado como par�metro. Controla la excepci�n
	 * {@code java.lang.ClassCastException} cuando un objeto de una clase no se
	 * puede castear a otro clase (por ejemplo el casteo del objeto de tipo Castillo
	 * a la clase Templo, porque no tienen los mismos par�metros). Muestra los
	 * mensajes correspondientes por consola.
	 */

	private static void anadirIvaAlPrecioCastillo() {

		if (hayMonumentos()) {

			Monumento monumento = gestor.buscarMonumento(pedirCodigo());

			if (monumento != null) {

				try {
					((Castillo) monumento).anadirIvaAlPrecio();
				} catch (ClassCastException exception) {
					System.out.println(
							"El monumento no es un castillo, no se puede a�adir el IVA al precio de las entradas");
				}

			}

		} else {
			mostrarMensajeListaMonumentosVacia();
		}

	}

	/**
	 * M�todo que devuelve true si la lista que almacena los monumentos
	 * {@code ArrayList<Monumento> listaMonumentos} est� vac�a.
	 * 
	 * @return true si la lista est� vac�a, false en el caso contrario
	 */

	private static boolean hayMonumentos() {

		if (gestor.getListaMonumentos().size() == 0) {
			return false;
		}

		return true;

	}

	/**
	 * M�todo que devuelve true si la lista que almacena los empleados
	 * {@code ArrayList<Empleado> listaEmpleados} est� vac�a.
	 * 
	 * @return true si la lista est� vac�a, false en el caso contrario
	 */

	private static boolean hayEmpleados() {

		if (gestor.getListaEmpleados().size() == 0) {
			return false;
		}

		return true;

	}

	/**
	 * M�todo que no recibe y no devuelve nada. Se llama a este m�todo si el
	 * {@code ArrayList<Monumento> listaMonumentos} de la instancia de la clase
	 * Ayuntamiento est� vac�o.
	 */

	private static void mostrarMensajeListaMonumentosVacia() {

		System.out.println("No hay monumentos dados de alta. Primero tienes que dar de alta a los monumentos");

	}

	/**
	 * M�todo que no recibe y no devuelve nada. Se llama a este m�todo si el
	 * {@code ArrayList<Empleado> listaEmpleados} de la instancia de la clase
	 * Ayuntamiento est� vac�o.
	 */

	private static void mostrarMensajeListaEmpleadosVacia() {

		System.out.println("No hay empleados dados de alta. Primero tienes que dar de alta a los empleados");

	}

	/**
	 * M�todo que no recibe y no devuelve nada. Se llama a este m�todo si los dos
	 * ArrayList de la instancia de la clase Ayuntamiento est�n vac�os.
	 */

	private static void mostrarMensajeListasVacias() {

		System.out.println("No hay empleados ni monumentos dados de alta. Primero tienes que darlos de alta");

	}

	/**
	 * M�todo que no recibe y no devuelve nada. Termina el programa. Antes de
	 * terminarlo, cierra el esc�ner y muestra un mensaje de salida
	 */

	private static void terminarPrograma() {

		System.out.println("\n- Programa terminado -");
		input.close();
		System.exit(0);
	}

}
