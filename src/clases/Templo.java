package clases;

/**
 * Clase Templo que contiene constructores necesarios para instanciar los
 * objetos de tipo Templo. Hereda de la superclase Monumento. Contiene m�todos
 * getters y setters, el m�todo toString(), y el m�todo propio
 * {@code calcularSuperficie()} para calcular la superficie del templo.
 * 
 * @author Klara Galuskova, 1� DAW
 */

public class Templo extends Monumento {

	private double ancho;
	private double largo;
	private String nombre;

	public Templo(String codigoMonumento, String tipo, double precio, double ancho, double largo, String nombre) {
		super(codigoMonumento, tipo);
		this.ancho = ancho;
		this.largo = largo;
		this.nombre = nombre;
		this.precio = precio;
	}

	public Templo(String codigoMonumento, String tipo, double precio) {
		super(codigoMonumento, tipo);
		this.precio = precio;
	}

	public Templo() {
		super();
		this.ancho = 0;
		this.largo = 0;
		this.precio = 0;
	}

	public double getAncho() {
		return ancho;
	}

	public void setAncho(double ancho) {
		this.ancho = ancho;
	}

	public double getLargo() {
		return largo;
	}

	public void setLargo(double largo) {
		this.largo = largo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Override
	public String toString() {
		return "Templo [codigoMonumento=" + codigoMonumento + ", listaEmpleados=" + listaEmpleados + ", tipo=" + tipo
				+ ", precio=" + precio + ", ancho=" + ancho + ", largo=" + largo + ", nombre=" + nombre + "]";
	}

	/**
	 * M�todo propio de la subclase Templo para calcular la superficie del templo.
	 * Usa los atributos propios de la subclase Templo, ancho y largo.
	 * 
	 * @return la superficie del templo (multiplicando el atributo ancho por el
	 *         atributo largo)
	 */
	public double calcularSuperficie() {
		return this.ancho * this.largo;
	}

}
