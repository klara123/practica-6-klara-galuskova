package clases;

import java.util.ArrayList;

/**
 * Clase Monumento que contiene constructores necesarios para instanciar los
 * objetos de tipo Monumento. Es la superclase de la que heredan las subclases
 * Templo y Castillo. Contiene m�todos getters y setters, el m�todo toString(),
 * y el m�todo propio {@code trabajaEnMonumento(String dni)} para comprobar si
 * un empleado trabaja en un monumento.
 * 
 * @author Klara Galuskova, 1� DAW
 */

public class Monumento {

	protected String codigoMonumento;
	protected ArrayList<Empleado> listaEmpleados;
	protected String tipo;
	protected double precio;

	public Monumento(String codigoMonumento, String tipo, double precio) {
		this.codigoMonumento = codigoMonumento;
		this.tipo = tipo;
		this.precio = precio;
		this.listaEmpleados = new ArrayList<Empleado>();
	}

	public Monumento(String codigoMonumento, String tipo) {
		this.codigoMonumento = codigoMonumento;
		this.tipo = tipo;
		this.listaEmpleados = new ArrayList<Empleado>();
	}

	public Monumento() {
		this.codigoMonumento = "";
		this.tipo = "";
		this.precio = 4.4;
		this.listaEmpleados = new ArrayList<Empleado>();
	}

	public String getCodigoMonumento() {
		return codigoMonumento;
	}

	public void setCodigoMonumento(String codigoMonumento) {
		this.codigoMonumento = codigoMonumento;
	}

	public ArrayList<Empleado> getListaEmpleados() {
		return listaEmpleados;
	}

	public void setListaEmpleados(ArrayList<Empleado> listaEmpleados) {
		this.listaEmpleados = listaEmpleados;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	@Override
	public String toString() {
		return "Monumento [codigoMonumento=" + codigoMonumento + ", listaEmpleados=" + listaEmpleados + ", tipo=" + tipo
				+ ", precio=" + precio + "]";
	}

	/**
	 * M�todo de la clase Monumento que determina si el empleado cuyo dni hemos
	 * pasado por par�metro se encuentra en la lista de empleados
	 * {@code ArrayList<Empleado>} listaEmpleados) de este monumento.
	 * 
	 * @param dni dni del empleado
	 * @return true si el empleado trabaja en el monumento (est� en el
	 *         {@code ArrayList<Empleado>} listaEmpleados de este monumento), false
	 *         si no trabaja en el monumento
	 */
	public boolean trabajaEnMonumento(String dni) {

		if (listaEmpleados.size() > 0) {
			for (Empleado empleado : this.listaEmpleados) {
				if (empleado != null && empleado.getDni().equals(dni)) {
					return true;
				}
			}
		}

		return false;

	}

}
