package clases;

/**
 * Clase Castillo que contiene constructores necesarios para instanciar los
 * objetos de tipo Castillo. Hereda de la superclase Monumento. Contiene m�todos
 * getters y setters, el m�todo toString(), y el m�todo propio
 * {@code anadirIvaAlPrecio()} para a�adir el IVA al precio que sobreescribe un
 * atributo heredado de la clase padre, Monumento.
 * 
 * @author Klara Galuskova, 1� DAW
 */

public class Castillo extends Monumento {

	/**
	 * {@value #IVA} Constante para guardar el valor del IVA que se usa en los
	 * m�todos de la clase Monumento.
	 */
	static final double IVA = 0.21;

	private String estilo;
	private int sigloConstruccion;
	private double altura;

	public Castillo(String codigoMonumento, String tipo, double precio, String estilo, int sigloConstruccion,
			double altura) {
		super(codigoMonumento, tipo);
		this.estilo = estilo;
		this.sigloConstruccion = sigloConstruccion;
		this.altura = altura;
		this.precio = precio;
	}

	public Castillo(String codigoMonumento, String tipo, double precio) {
		super(codigoMonumento, tipo);
		this.precio = precio;
	}

	public Castillo() {
		super();
		this.estilo = "";
		this.sigloConstruccion = 0;
		this.altura = 0;
		this.precio = 0;
	}

	public String getEstilo() {
		return estilo;
	}

	public void setEstilo(String estilo) {
		this.estilo = estilo;
	}

	public int getSigloConstruccion() {
		return sigloConstruccion;
	}

	public void setSigloConstruccion(int sigloConstruccion) {
		this.sigloConstruccion = sigloConstruccion;
	}

	public double getAltura() {
		return altura;
	}

	public void setAltura(double altura) {
		this.altura = altura;
	}

	@Override
	public String toString() {
		return "Castillo [codigoMonumento=" + codigoMonumento + ", listaEmpleados=" + listaEmpleados + ", tipo=" + tipo
				+ ", precio=" + precio + ", estilo=" + estilo + ", sigloConstruccion=" + sigloConstruccion + ", altura="
				+ altura + "]";
	}

	/**
	 * M�todo propio de la subclase Castillo. A�ade el IVA al atributo precio, y
	 * sobreesribe el atributo precio heredado de la clase padre Monumento.
	 */
	public void anadirIvaAlPrecio() {

		double precio = this.getPrecio();
		precio += precio *= IVA;
		this.setPrecio(precio);

		System.out.println("IVA a�adido al precio. El nuevo precio es: " + this.getPrecio() + " �");

	}

}
