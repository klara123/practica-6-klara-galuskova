package clases;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Clase Ayuntamiento en la que se guardan ArrayList que contienen instancias de
 * las clases Empleado y Monumento. Contiene m�todos para gestionar las
 * instancias de esas clases, un constructor, y los m�todos getters y setters
 * para sus atributos.
 * 
 * @author Klara Galuskova, 1� DAW
 */

public class Ayuntamiento {

	private ArrayList<Empleado> listaEmpleados;
	private ArrayList<Monumento> listaMonumentos;

	public Ayuntamiento() {
		this.listaEmpleados = new ArrayList<Empleado>();
		this.listaMonumentos = new ArrayList<Monumento>();
	}

	public ArrayList<Empleado> getListaEmpleados() {
		return listaEmpleados;
	}

	public void setListaEmpleados(ArrayList<Empleado> listaEmpleados) {
		this.listaEmpleados = listaEmpleados;
	}

	public ArrayList<Monumento> getListaMonumentos() {
		return listaMonumentos;
	}

	public void setListaMonumentos(ArrayList<Monumento> listaMonumentos) {
		this.listaMonumentos = listaMonumentos;
	}

	/**
	 * M�todo para dar de alta a un empleado (a�adirlo al ArrayList
	 * {@code ArrayList<Empleado> listaEmpleados}). Crea una nueva instancia de la
	 * clase Empleado, llamando a su constructor usando los atributos pasados por
	 * par�metro. Hace una llamada al m�todo {@code existeEmpleado(String dni)},
	 * para evitar duplicidad (el DNI de cada empleado es �nico).
	 * 
	 * @param nombre   nombre del empleado
	 * @param apellido apellido del empleado
	 * @param dni      DNI del empleado (no se puede repetir)
	 */

	public void altaEmpleado(String nombre, String apellido, String dni) {

		if (!existeEmpleado(dni)) {
			Empleado nuevoEmpleado = new Empleado(nombre, apellido, dni);
			listaEmpleados.add(nuevoEmpleado);
			System.out.println("Empleado con el DNI '" + dni + "' dado de alta");
		} else {
			System.out.println("Empleado no se puede dar de alta. DNI '" + dni + "' duplicado");
		}

	}

	/**
	 * M�todo para dar de alta a un monumento (a�adirlo al ArrayList
	 * {@code ArrayList<Monumento> listaMonumentos}). Crea una nueva instancia de la
	 * clase Monumento, llamando a su constructor usando los atributos pasados por
	 * par�metro. Hace una llamada al m�todo
	 * {@code existeMonumento(String codigoMonumento)}, para evitar duplicidad (el
	 * c�digo de cada monumento es �nico).
	 * 
	 * @param codigoMonumento c�digo del monumento (no se puede repetir)
	 * @param tipo            tipo del monumento (ej. iglesia)
	 * @param precio          precio de las entradas
	 */

	public void altaMonumento(String codigoMonumento, String tipo, double precio) {

		if (!existeMonumento(codigoMonumento)) {
			Monumento nuevoMonumento = new Monumento(codigoMonumento, tipo, precio);
			listaMonumentos.add(nuevoMonumento);
			System.out.println("Monumento con el c�digo '" + codigoMonumento + "' dado de alta");
		} else {
			System.out.println("Monumento no se puede dar de alta. C�digo '" + codigoMonumento + "' duplicado");
		}

	}

	/**
	 * M�todo para dar de alta a un templo (a�adirlo al ArrayList
	 * {@code ArrayList<Monumento> listaMonumentos}). Crea una nueva instancia de la
	 * clase Templo, llamando a su constructor usando los atributos pasados por
	 * par�metro. Hace una llamada al m�todo
	 * {@code existeMonumento(String codigoMonumento)}, para evitar duplicidad (el
	 * c�digo de cada monumento es �nico).
	 * 
	 * @param codigoMonumento c�digo del monumento (no se puede repetir)
	 * @param tipo            tipo del monumento (ej. iglesia)
	 * @param precio          precio de las entradas
	 * @param ancho           ancho del templo
	 * @param largo           largo del templo
	 * @param nombre          nombre del templo (ej. Bas�lica del Pilar de Zaragoza)
	 */

	public void altaTemplo(String codigoMonumento, String tipo, double precio, double ancho, double largo,
			String nombre) {

		if (!existeMonumento(codigoMonumento)) {
			Templo nuevoTemplo = new Templo(codigoMonumento, tipo, precio, ancho, largo, nombre);
			listaMonumentos.add(nuevoTemplo);
			System.out.println("Templo con el c�digo '" + codigoMonumento + "' dado de alta");
		} else {
			System.out.println("Templo no se puede dar de alta. C�digo '" + codigoMonumento + "' duplicado");
		}

	}

	/**
	 * M�todo para dar de alta a un castillo (a�adirlo al ArrayList
	 * {@code ArrayList<Monumento> listaMonumentos}). Crea una nueva instancia de la
	 * clase Castillo, llamando a su constructor usando los atributos pasados por
	 * par�metro. Hace una llamada al m�todo
	 * {@code existeMonumento(String codigoMonumento)}, para evitar duplicidad (el
	 * c�digo de cada monumento es �nico).
	 * 
	 * @param codigoMonumento   c�digo del monumento (no se puede repetir)
	 * @param tipo              tipo del monumento (ej. iglesia)
	 * @param precio            precio de las entradas
	 * @param estilo            estilo del monumento (ej. g�tico)
	 * @param sigloConstruccion siglo de construcci�n (ej. 12)
	 * @param altura            altura del castillo
	 */

	public void altaCastillo(String codigoMonumento, String tipo, double precio, String estilo, int sigloConstruccion,
			double altura) {

		if (!existeMonumento(codigoMonumento)) {
			Castillo nuevoCastillo = new Castillo(codigoMonumento, tipo, precio, estilo, sigloConstruccion, altura);
			listaMonumentos.add(nuevoCastillo);
			System.out.println("Castillo con el c�digo '" + codigoMonumento + "' dado de alta");
		} else {
			System.out.println("Castillo no se puede dar de alta. C�digo '" + codigoMonumento + "' duplicado");
		}

	}

	/**
	 * M�todo para comprobar si el objeto Empleado se encuentra en la lista de
	 * empleados del gestor ({@code ArrayList<Empleado> listaEmpleados}).
	 * 
	 * @param dni DNI del empleado
	 * @return true si el empleado est� incluido en el
	 *         ({@code ArrayList<Empleado> listaEmpleados}), false en el caso
	 *         contrario
	 */

	private boolean existeEmpleado(String dni) {

		for (Empleado empleado : listaEmpleados) {
			if (empleado != null && empleado.getDni().equals(dni)) {
				return true;
			}
		}

		return false;
	}

	/**
	 * M�todo para comprobar si el objeto Monumento se encuentra en la lista de
	 * monumentos del gestor ({@code ArrayList<Monumento> listaMonumentos}).
	 * 
	 * @param codigoMonumento c�digo del monumento
	 * @return true si el monumento est� incluido en el
	 *         ({@code ArrayList<Monumento> listaMonumentos}), false en el caso
	 *         contrario
	 */

	private boolean existeMonumento(String codigoMonumento) {

		for (Monumento monumento : listaMonumentos) {
			if (monumento != null && monumento.getCodigoMonumento().equals(codigoMonumento)) {
				return true;
			}
		}

		return false;
	}

	/**
	 * M�todo que recibe el DNI del empleado que queremos encontrar, y devuelve el
	 * objeto de tipo Empleado. Si no encuentra el empleado con este DNI, devuelve
	 * null.
	 * 
	 * @param dni DNI del empleado
	 * @return objeto empleado si encuentra el empleado con el DNI, null si no lo
	 *         encuentra
	 */

	public Empleado buscarEmpleado(String dni) {

		for (Empleado empleado : listaEmpleados) {

			if (empleado != null && empleado.getDni().equals(dni)) {
				return empleado;
			}

		}

		System.out.println("Empleado con el DNI '" + dni + "' no encontrado");

		return null;

	}

	/**
	 * M�todo que recibe el c�digo del monumento que queremos encontrar, y devuelve
	 * el objeto de tipo Monumento. Si no encuentra el monumento con este c�digo,
	 * devuelve null.
	 * 
	 * @param codigoMonumento el c�digo del monumento
	 * @return monumento si encuentra el monumento con el c�digo, null si no lo
	 *         encuentra
	 */

	public Monumento buscarMonumento(String codigoMonumento) {

		for (Monumento monumento : listaMonumentos) {

			if (monumento != null && monumento.getCodigoMonumento().equals(codigoMonumento)) {
				return monumento;
			}

		}

		System.out.println("Monumento con el c�digo '" + codigoMonumento + "' no encontrado");

		return null;

	}

	/**
	 * M�todo que no recibe par�metros y que no devuelve nada. Lista los empleados
	 * almacenados en la lista {@code ArrayList<Empleado> listaEmpleados}. Si el empleado
	 * es null, no lo lista.
	 */

	public void listarEmpleados() {

		int counter = 0;

		for (Empleado empleado : listaEmpleados) {
			if (empleado != null) {
				counter++;
				System.out.println(counter + " - " + empleado);
			}
		}

	}

	/**
	 * M�todo que no recibe par�metros y que no devuelve nada. Lista los monumentos
	 * almacenados en la lista {@code ArrayList<Monumento> listaMonumentos}. Si el
	 * monumento es null, no lo lista.
	 */

	public void listarMonumentos() {

		int counter = 0;

		for (Monumento monumento : listaMonumentos) {
			if (monumento != null) {
				counter++;
				System.out.println(counter + " - " + monumento);
			}
		}

	}

	/**
	 * M�todo que elimina el empleado cuyo DNI hemos pasado por par�metro. Devuelve
	 * un boolean true/false seg�n si ha encontrado y eliminado el empleado o no.
	 * Llama al m�todo {@code eliminarEmpleadoDelMonumento(String dni)}, para
	 * eliminar el empleado tambi�n de los monumentos en cuya lista de empleados
	 * est� almacenado (en los monumentos d�nde est� trabajando).
	 * 
	 * @param dni DNI del empleado
	 * @return true si ha eliminado el empleado, false si no ha encontrado el
	 *         empleado
	 */

	public boolean eliminarEmpleado(String dni) {

		Iterator<Empleado> iterator = listaEmpleados.iterator();

		while (iterator.hasNext()) {

			Empleado empleado = iterator.next();

			if (empleado != null && empleado.getDni().equals(dni)) {
				eliminarEmpleadoDelMonumento(dni);
				iterator.remove();
				return true;
			}

		}

		return false;

	}

	/**
	 * M�todo que elimina el empleado cuyo DNI hemos pasado por par�metro de la
	 * lista de empleados de cada monumento d�nde est� trabajando. Se llama desde el
	 * m�todo {@code eliminarEmpleado(String dni)}. Recibe el DNI del empleado que
	 * estamos eliminando, y no devuelve nada.
	 * 
	 * @param dni DNI del empleado
	 */

	private void eliminarEmpleadoDelMonumento(String dni) {

		for (Monumento monumento : listaMonumentos) {

			if (monumento != null) {

				Iterator<Empleado> iterator = monumento.getListaEmpleados().iterator();

				while (iterator.hasNext()) {

					Empleado empleado = iterator.next();

					if (empleado != null && empleado.getDni().equals(dni)) {
						iterator.remove();
						System.out.println("Empleado '" + dni + "' eliminado de la lista de empleados del monumento '"
								+ monumento.getCodigoMonumento() + "'");
					}

				}

			}

		}

	}

	/**
	 * M�todo que elimina el monumento cuyo c�digo hemos pasado por par�metro.
	 * Devuelve un boolean true/false seg�n si ha encontrado y eliminado el
	 * monumento o no.
	 * 
	 * @param codigoMonumento c�digo del monumento
	 * @return true si ha eliminado el monumento, false si no ha encontrado el
	 *         monumento
	 */

	public boolean eliminarMonumento(String codigoMonumento) {

		Iterator<Monumento> iterator = listaMonumentos.iterator();

		while (iterator.hasNext()) {

			Monumento monumento = iterator.next();

			if (monumento != null && monumento.getCodigoMonumento().equals(codigoMonumento)) {
				iterator.remove();
				return true;
			}

		}

		return false;

	}

	/**
	 * M�todo que asigna un empleado a un monumento. Llama a los m�todos
	 * buscarEmpleado(String dni) y buscarMonumento(String codigo) para encontrar
	 * los dos objetos. Si no existe ninguno o solo existe uno de ellos, nos muestra
	 * el mensaje correspondiente por consola. Una vez hecha esta comprobaci�n,
	 * llama al m�todo trabajaEnMonumento(String dni), que comprueba si el mismo
	 * empleado ya est� asignado al monumento o no, y as� evitar empleados
	 * duplicados en el mismo monumento. Despu�s de hacer esta �ltima comprobaci�n,
	 * a�ade el empleado al {@code ArrayList<Empleado> listaEmpleados} del
	 * monumento.
	 * 
	 * @param dni             DNI del empleado
	 * @param codigoMonumento c�digo del monumento
	 * @return true si ha asignado el empleado al monumento, en el caso contario
	 *         devuelve false
	 */
	
	public boolean asignarEmpleadoMonumento(String dni, String codigoMonumento) {

		if (existeEmpleado(dni) && existeMonumento(codigoMonumento)) {

			Monumento monumento = buscarMonumento(codigoMonumento);

			if (!monumento.trabajaEnMonumento(dni)) {
				Empleado empleado = buscarEmpleado(dni);
				monumento.getListaEmpleados().add(empleado);
				System.out.println("Empleado con el DNI '" + dni + "' asignado al monumento '" + codigoMonumento + "'");
				return true;
			} else {
				System.out.println("El empleado '" + dni + "' no se puede asignar. Ya trabaja en el monumento '"
						+ codigoMonumento + "'");
			}

		} else if (!existeEmpleado(dni) && !existeMonumento(codigoMonumento)) {
			System.out.println("El monumento y el empleado no existen");
		} else if (existeMonumento(codigoMonumento)) {
			System.out.println("El empleado con el DNI '" + dni + "' no existe");
		} else {
			System.out.println("El monumento con el c�digo '" + codigoMonumento + "' no existe");
		}

		return false;

	}

}
