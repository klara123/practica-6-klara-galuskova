package clases;

import java.time.LocalDate;

/**
 * Clase Empleado que contiene constructores necesarios para instanciar los
 * objetos de tipo Empleado. Contiene m�todos getters y setters, y el m�todo
 * toString().
 * 
 * @author Klara Galuskova, 1� DAW
 */

public class Empleado {

	private String nombre;
	private String apellido;
	private String dni;
	private LocalDate inicioContrato;
	private LocalDate finContrato;

	public Empleado() {
		this.nombre = "";
		this.apellido = "";
		this.dni = "";
		this.inicioContrato = LocalDate.now();
	}

	public Empleado(String nombre, String apellido, String dni) {
		this.nombre = nombre;
		this.apellido = apellido;
		this.dni = dni;
		this.inicioContrato = LocalDate.now();
		this.finContrato = inicioContrato.plusYears(5);
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public LocalDate getInicioContrato() {
		return inicioContrato;
	}

	public void setInicioContrato(LocalDate inicioContrato) {
		this.inicioContrato = inicioContrato;
	}

	public LocalDate getFinContrato() {
		return finContrato;
	}

	public void setFinContrato(LocalDate finContrato) {
		this.finContrato = finContrato;
	}

	@Override
	public String toString() {
		return "Empleado [nombre=" + nombre + ", apellido=" + apellido + ", dni=" + dni + ", inicioContrato="
				+ inicioContrato + ", finContrato=" + finContrato + "]";
	}

}
